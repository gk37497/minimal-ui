// src/components/Button.tsx

import React, { FC } from 'react';

type Props = {
  onClick?: () => void;
  disabled?: boolean;
  children: React.ReactNode;
};

const Button: FC<Props> = ({ children, onClick, disabled }) => {
  return (
    <button onClick={onClick} disabled={disabled}>
      {children}GGGG
    </button>
  );
};

export default Button;
