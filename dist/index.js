'use strict';

var React = require('react');

// src/components/Button.tsx
var Button = function (_a) {
    var children = _a.children, onClick = _a.onClick, disabled = _a.disabled;
    return (React.createElement("button", { onClick: onClick, disabled: disabled },
        children,
        "GGGG"));
};

exports.Button = Button;
