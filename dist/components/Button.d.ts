import React, { FC } from 'react';
type Props = {
    onClick?: () => void;
    disabled?: boolean;
    children: React.ReactNode;
};
declare const Button: FC<Props>;
export default Button;
